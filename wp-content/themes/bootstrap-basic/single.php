<?php
/**
 * Template for displaying single post (read full post page).
 * 
 * @package bootstrap-basic
 */

get_header();

/**
 * determine main column size from actived sidebar
 */
$main_column_size = bootstrapBasicGetMainColumnSize();
?> 
<?php get_sidebar('left'); ?>

            <div class="container">
                <div class="row">
                    <div class="col-md-9 content-area" id="main-column">
                        <main id="main" class="site-main" role="main">
                            <?php


                            while (have_posts()) {

                                if (in_category( 'htmlcss' )) {
                                    echo '<div class="main-info-wrapper">';
                                    echo    '<div class="hotel-name">';
                                    echo       get_post_meta(get_the_ID(), "curseName", true);
                                    echo    '</div>';
                                    echo '</div>';
                                    the_post();
                                    echo '<ul class="nav nav-tabs">';
                                    echo   '<li class="active"><a href="#curseDescription">Опис курсу:</a></li>';
                                    echo   '<li><a href="#afterCurse">Після вивчення курсу Ви зможете:</a></li>';
                                    echo   '<li><a href="#curseProgram">Програма курсу:</a></li>';
                                    echo   '<li><a href="#minRequirements">Мінімальні вимоги:</a></li>';
                                    echo   '<li><a href="#prise">Ціна і Дати</a></li>';
                                    echo '</ul>';
                                    echo '<div class="tab-content">';
                                    echo   '<div id="curseDescription" class="tab-pane active">';
                                    echo        '<div class="ul-wrapper">';
                                    echo             get_template_part('content', get_post_format());
                                    echo        '</div>';
                                    echo   '</div>';
                                    echo   '<div id="afterCurse" class="tab-pane fade">';
                                    echo        '<div class="ul-wrapper">';
                                    echo            get_post_meta(get_the_ID(), "after", true);
                                    echo        '</div>';
                                    echo   '</div>';
                                    echo   '<div id="curseProgram" class="tab-pane fade">';
                                    echo         get_post_meta(get_the_ID(), "program", true);
                                    echo   '</div>';
                                    echo   '<div id="minRequirements" class="tab-pane fade">';
                                    echo        '<div class="ul-wrapper">';
                                    echo             get_post_meta(get_the_ID(), "requirements", true);
                                    echo        '</div>';
                                    echo   '</div>';
                                    echo   '<div id="prise" class="tab-pane fade">';
                                    echo       '<div class="prise">';
                                    echo          '<h4>Ціна:</h4> ' . get_post_meta(get_the_ID(), "prise", true);
                                    echo       '</div>';
                                    echo       '<div class="data-time">';
                                    echo          '<h4>Дати:</h4> ' . get_post_meta(get_the_ID(), "data", true);
                                    echo       '</div>';
                                    echo   '</div>';
                                    echo '</div>';
                                } else  if (in_category( 'javascript' )){
                                    echo '<div class="main-info-wrapper">';
                                    echo    '<div class="hotel-name">';
                                    echo       get_post_meta(get_the_ID(), "curseName", true);
                                    echo    '</div>';
                                    echo '</div>';
                                    the_post();
                                    echo '<ul class="nav nav-tabs">';
                                    echo   '<li class="active"><a href="#curseDescription">Опис курсу:</a></li>';
                                    echo   '<li><a href="#afterCurse">Після вивчення курсу Ви зможете:</a></li>';
                                    echo   '<li><a href="#curseProgram">Програма курсу:</a></li>';
                                    echo   '<li><a href="#minRequirements">Мінімальні вимоги:</a></li>';
                                    echo   '<li><a href="#prise">Ціна і Дати</a></li>';
                                    echo '</ul>';
                                    echo '<div class="tab-content">';
                                    echo   '<div id="curseDescription" class="tab-pane active">';
                                    echo        '<div class="ul-wrapper">';
                                    echo             get_template_part('content', get_post_format());
                                    echo        '</div>';
                                    echo   '</div>';
                                    echo   '<div id="afterCurse" class="tab-pane fade">';
                                    echo        '<div class="ul-wrapper">';
                                    echo            get_post_meta(get_the_ID(), "after", true);
                                    echo        '</div>';
                                    echo   '</div>';
                                    echo   '<div id="curseProgram" class="tab-pane fade">';
                                    echo         get_post_meta(get_the_ID(), "program", true);
                                    echo   '</div>';
                                    echo   '<div id="minRequirements" class="tab-pane fade">';
                                    echo        '<div class="ul-wrapper">';
                                    echo             get_post_meta(get_the_ID(), "requirements", true);
                                    echo        '</div>';
                                    echo   '</div>';
                                    echo   '<div id="prise" class="tab-pane fade">';
                                    echo       '<div class="prise">';
                                    echo          '<h4>Ціна:</h4> ' . get_post_meta(get_the_ID(), "prise", true);
                                    echo       '</div>';
                                    echo       '<div class="data-time">';
                                    echo          '<h4>Дати:</h4> ' . get_post_meta(get_the_ID(), "data", true);
                                    echo       '</div>';
                                    echo   '</div>';
                                    echo '</div>';
                                }else{
                                    the_post();
                                    get_template_part('content', get_post_format());
                                }




                                echo "\n\n";

                                bootstrapBasicPagination();

                                echo "\n\n";

                                // If comments are open or we have at least one comment, load up the comment template
                                if (comments_open() || '0' != get_comments_number()) {
                                    comments_template();
                                }


                                echo "\n\n";
                            };
                            ?>
                        </main>
                    </div>
                    <div class="col-md-3">
                        <?php get_sidebar('right'); ?>
                    </div>
                </div>

            </div>


<?php get_footer(); ?> 
