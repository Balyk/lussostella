<?php
/**
 * The theme header
 *
 * @package bootstrap-basic
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <!--wordpress head-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<!--[if lt IE 8]>
<p class="ancient-browser-alert">You are using an <strong>outdated</strong> browser. Please <a
        href="https://browsehappy.com/" target="_blank">upgrade your browser</a>.</p>
<![endif]-->


<div class="container-fluid page-container">
    <?php do_action('before'); ?>
    <header role="banner">
        <div class="header-fixed">
            <div class="top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col d-flex flex-row">
                            <div class="phone">+38 093 126 4745</div>
                            <div class="social">
                                <ul class="social-list">
                                    <li class="social-list-item"><a href="#"><i class="fa fa-instagram"
                                                                                aria-hidden="true"></i></a></li>
                                    <li class="social-list-item"><a href="#"><i class="fa fa-facebook"
                                                                                aria-hidden="true"></i></a></li>
                                    <li class="social-list-item"><a href="#"><i class="fa fa-linkedin"
                                                                                aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                            <div class="user-box ml-auto">
                                <div class="user-box-login user-box-link"><a href="#">знижки</a></div>
                                <div class="user-box-register user-box-link"><a href="#">акції</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="col-md-12">

                    <div class="flex-wrapper">
                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#contact">
                            Замовити
                        </button>

                        <div class="right-menu ml-auto">
                            <nav class="navigation" role="navigation">
                                <?php wp_nav_menu(array('theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav', 'walker' => new BootstrapBasicMyWalkerNavMenu())); ?>
                                <?php dynamic_sidebar('navbar-right'); ?>

                            </nav>
                            <div class="hamburger">
                                <i class="fa fa-bars trans_200"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (is_front_page()) {
            echo '<div class="home">
                <div class="main-image">
                <h1 class="section-title">LussoStella <span>Унікальні запрошення у вигляді сайту на Весілля,День Народження та інші святкові події</span></h1>
                    <img class="first-flower" src="/wp-content/themes/bootstrap-basic/img/1.webp" alt="">
                    <img class="main-flowers" src="/wp-content/themes/bootstrap-basic/img/2.webp" alt="">
                    <img class="second-flower" src="/wp-content/themes/bootstrap-basic/img/3.webp" alt="">
                </div>
                 
                  </div>';
//            wp_nav_menu( array(
//                'theme_location' => 'my-custom-menu',
//                'container_class' => 'custom-menu-class' ) );
//            echo '</div></div></div></div></div>';
        } else if (is_category()) {
            echo '<div class="header-page">';
            echo '<div class="entry-title">';
            the_title();
            echo '</div>';
            echo '</div>';
        } else {
            echo '<div class="header-page">';
            echo '<div class="entry-title">';
            the_title();
            echo '</div>';
            bootstrapbasic_post_thumbnail();
            echo '</div>';
        } ?>
       
    </header>
    <div class="menu-mobile">
        <div class="menu-content d-flex flex-column align-items-center justify-content-center text-center">
            <div class="logo"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><img
                            src="<?php echo esc_url(home_url('/')); ?>wp-content/themes/bootstrap-basic/img/logo.svg"
                            alt="LUSSOSTELLA"></a></div>
            <?php wp_nav_menu(array('theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav', 'walker' => new BootstrapBasicMyWalkerNavMenu())); ?>
            <?php dynamic_sidebar('navbar-right'); ?>
        </div>
    </div>


    <div id="content" class="site-content container-fluid">
<!--        <div class="row">-->
