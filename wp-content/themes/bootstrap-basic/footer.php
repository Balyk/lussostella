<?php

?>
<!--                </div><!--.row-->
</div><!--.site-content-->
<?php if (is_front_page()) {
    dynamic_sidebar('front-posts');
    dynamic_sidebar('contact');
}else{
    dynamic_sidebar('contact-time');
}?>
<footer id="site-footer" role="contentinfo">
    <div class="main-footer">
        <div class="container">
            <div id="footer-row" class="row">
                <div class="col-md-6 footer-left">
                    <?php dynamic_sidebar('footer-left'); ?>
                </div>
                <div class="col-md-6 footer-right">
                    <?php dynamic_sidebar('footer-right'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flex-wrapper">
                        <?php dynamic_sidebar('footer-copyright'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

</div><!--.container page-container-->


<!--wordpress footer-->
<?php wp_footer(); ?>
<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="contact">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Замовити сайт Запрошення</h4>
            </div>
            <div class="modal-body">
                Ти Мега Монстр
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>