<?php
/**
 * Bootstrap Basic theme
 *
 * @package bootstrap-basic
 */


/**
 * Required WordPress variable.
 */
if (!isset($content_width)) {
    $content_width = 1170;
}


if (!function_exists('bootstrapBasicSetup')) {
    /**
     * Setup theme and register support wp features.
     */
    function bootstrapBasicSetup()
    {
        /**
         * Make theme available for translation
         * Translations can be filed in the /languages/ directory
         *
         * copy from underscores theme
         */
        load_theme_textdomain('bootstrap-basic', get_template_directory() . '/languages');

        // add theme support title-tag
        add_theme_support('title-tag');

        // add theme support post and comment automatic feed links
        add_theme_support('automatic-feed-links');

        // enable support for post thumbnail or feature image on posts and pages
        add_theme_support('post-thumbnails');

        // allow the use of html5 markup
        // @link https://codex.wordpress.org/Theme_Markup
        add_theme_support('html5', array('caption', 'comment-form', 'comment-list', 'gallery', 'search-form'));

        // add support menu
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'bootstrap-basic'),
        ));

        // add post formats support
        add_theme_support('post-formats', array('aside', 'image', 'video', 'quote', 'link'));

        // add support custom background
        add_theme_support(
            'custom-background',
            apply_filters(
                'bootstrap_basic_custom_background_args',
                array(
                    'default-color' => 'ffffff',
                    'default-image' => ''
                )
            )
        );
    }// bootstrapBasicSetup
}
add_action('after_setup_theme', 'bootstrapBasicSetup');


if (!function_exists('bootstrapBasicWidgetsInit')) {
    /**
     * Register widget areas
     */
    function bootstrapBasicWidgetsInit()
    {
        register_sidebar(array(
            'name' => __('Sidebar right', 'bootstrap-basic'),
            'id' => 'sidebar-right',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));

        register_sidebar(array(
            'name' => __('Sidebar left', 'bootstrap-basic'),
            'id' => 'sidebar-left',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));

        register_sidebar(array(
            'name' => __('Header right', 'bootstrap-basic'),
            'id' => 'header-right',
            'description' => __('Header widget area on the right side next to site title.', 'bootstrap-basic'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));

        register_sidebar(array(
            'name' => __('Navigation bar right', 'bootstrap-basic'),
            'id' => 'navbar-right',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        ));

        register_sidebar(array(
            'name' => __('Footer left', 'bootstrap-basic'),
            'id' => 'footer-left',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));

        register_sidebar(array(
            'name' => __('Footer right', 'bootstrap-basic'),
            'id' => 'footer-right',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));
        register_sidebar(array(
            'name' => __('Footer CopyRight', 'bootstrap-basic'),
            'id' => 'footer-copyright',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));
        register_sidebar(array(
            'name' => __('Contact Section', 'bootstrap-basic'),
            'id' => 'contact',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));
        register_sidebar(array(
            'name' => __('Contact Time', 'bootstrap-basic'),
            'id' => 'contact-time',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));
        register_sidebar(array(
            'name' => __('Map', 'bootstrap-basic'),
            'id' => 'map',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));
        register_sidebar(array(
            'name' => __('Front Posts', 'bootstrap-basic'),
            'id' => 'front-posts',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h1 class="widget-title">',
            'after_title' => '</h1>',
        ));
    }// bootstrapBasicWidgetsInit
}
add_action('widgets_init', 'bootstrapBasicWidgetsInit');


if (!function_exists('bootstrapBasicEnqueueScripts')) {
    /**
     * Enqueue scripts & styles
     */
    function bootstrapBasicEnqueueScripts()
    {
        global $wp_scripts;

        wp_enqueue_style('fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0');
        wp_enqueue_style('OwlCarousel', get_template_directory_uri() . '/js/plugins/OwlCarousel2-2.2.1/owl.carousel.css', array(), '2.2.1');
        wp_enqueue_style('OwlCarousel', get_template_directory_uri() . '/js/plugins/OwlCarousel2-2.2.1/owl.theme.default.css', array(), '2.2.1');
        wp_enqueue_style('OwlCarousel', get_template_directory_uri() . '/js/plugins/OwlCarousel2-2.2.1/animate.css', array(), '3.5.1');

        wp_enqueue_style('style', get_template_directory_uri() . '/css/style.css');

        wp_enqueue_script('modernizr-script', get_template_directory_uri() . '/js/vendor/modernizr.min.js', array(), '3.3.1');
        wp_register_script('respond-script', get_template_directory_uri() . '/js/vendor/respond.min.js', array(), '1.4.2');
        $wp_scripts->add_data('respond-script', 'conditional', 'lt IE 9');
        wp_enqueue_script('respond-script');
        wp_register_script('html5-shiv-script', get_template_directory_uri() . '/js/vendor/html5shiv.min.js', array(), '3.7.3');
        $wp_scripts->add_data('html5-shiv-script', 'conditional', 'lte IE 9');
        wp_enqueue_script('html5-shiv-script');
        wp_enqueue_script('jquery-main', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', array(), '3.2.1', true);
        wp_enqueue_script('bootstrap-script', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', array(), '3.3.7', true);
        wp_enqueue_script('OwlCarousel2', get_template_directory_uri() . '/js/plugins/OwlCarousel2-2.2.1/owl.carousel.js', array(), '2.2.1', true);
        wp_enqueue_script('ScrollMagic', get_template_directory_uri() . '/js/plugins/ScrollMagic.min.js', array(), '2.0.5', true);
        wp_enqueue_script('animation-gsap', get_template_directory_uri() . '/js/plugins/greensock/animation.gsap.min.js', array(), '2.0.5 ', true);
        wp_enqueue_script('scroll-to-plugin', get_template_directory_uri() . '/js/plugins/greensock/ScrollToPlugin.min.js', array(), '1.8.1 ', true);
        wp_enqueue_script('TimeLineMax', get_template_directory_uri() . '/js/plugins/greensock/TimelineMax.min.js', array(), '1.19.1 ', true);
        wp_enqueue_script('TweenMax', get_template_directory_uri() . '/js/plugins/greensock/TweenMax.min.js', array(), '1.19.1 ', true);
        wp_enqueue_script('easing', get_template_directory_uri() . '/js/plugins.js', array(), '1.3', true);
        wp_enqueue_script('main-script', get_template_directory_uri() . '/js/main.js', array(), false, true);
    }// bootstrapBasicEnqueueScripts
}
add_action('wp_enqueue_scripts', 'bootstrapBasicEnqueueScripts');


/**
 * admin page displaying help.
 */
if (is_admin()) {
    require get_template_directory() . '/inc/BootstrapBasicAdminHelp.php';
    $bbsc_adminhelp = new BootstrapBasicAdminHelp();
    add_action('admin_menu', array($bbsc_adminhelp, 'themeHelpMenu'));
    unset($bbsc_adminhelp);
}


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';


/**
 * Custom dropdown menu and navbar in walker class
 */
require get_template_directory() . '/inc/BootstrapBasicMyWalkerNavMenu.php';


/**
 * Template functions
 */
require get_template_directory() . '/inc/template-functions.php';


/**
 * --------------------------------------------------------------
 * Theme widget & widget hooks
 * --------------------------------------------------------------
 */
require get_template_directory() . '/inc/widgets/BootstrapBasicSearchWidget.php';
require get_template_directory() . '/inc/template-widgets-hook.php';

function bootstrapbasic_post_thumbnail()
{
    if (post_password_required() || is_attachment() || !has_post_thumbnail()) {
        return;
    }

    if (is_singular()) :
        ?>

        <div class="post-thumbnail">
            <?php the_post_thumbnail(); ?>
        </div><!-- .post-thumbnail -->

    <?php else : ?>

        <a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
            <?php
            the_post_thumbnail('post-thumbnail', array('alt' => get_the_title()));
            ?>
        </a>

    <?php endif; // End is_singular()
}

function wp_custom_new_menu()
{
    register_nav_menu('my-custom-menu', __('My Custom Menu'));
}

add_action('init', 'wp_custom_new_menu');


function program_func_wedding()
{
    query_posts(array('category_name' => 'wedding', 'posts_per_page' => -3));
    print('<section class="wedding-section">');
    print('<div class="container">');
    print('<div class="row">');
    print('<div class="col-md-12">');
    print('<h2 class="section-title">Весілля</h2>');
    print('<div class="wedding-wrapper" >');
    while (have_posts()) : the_post();
        echo '<div class="wedding-item-wrapper" >';
        echo '<div class="wedding-content">';
        echo '<div class="post-title">';
        echo the_title();
        echo '</div>';
        echo '<p class="post-text">';
        echo  wp_trim_words(get_the_excerpt(), 30, '...');
        echo '</p>';
        echo '<div class="post-footer">';
        echo '<a href="' . get_permalink() . '" class="button"><div class="button_bcg"></div>Детальніше<span></span></a>';
        echo '</div>';
        echo '</div>';
        echo '<div class="wedding-item-image" style="background:url(' . get_the_post_thumbnail_url() . ');"></div>';
        echo '</div>'; //htmlcss-item-wrapper
    endwhile;
    print('</div>'); //wedding-wrapper
    print('</div>'); //col-md-12
    print('</div>'); //row
    print('</div>'); //container
    print('</section>'); //wedding-section
    echo '';

    wp_reset_query();
}

add_shortcode('wedding', 'program_func_wedding');

function program_func_wedding_category()
{
    query_posts(array('category_name' => 'wedding', 'posts_per_page' => -3));
    print('<section class="wedding-section">');
    print('<div class="container">');
    print('<div class="row">');
    print('<div class="col-md-12">');
    print('<h2 class="section-title">222</h2>');
    while (have_posts()) : the_post();
        echo '<div class="wedding-item-wrapper" >';
        echo '<div class="wedding-content">';
        echo '<div class="post-title">';
        echo the_title();
        echo '</div>';
        echo '<p class="post-text">';
        echo get_the_content();
        echo '</p>';
        echo '<div class="post-footer">';
        echo '<a href="' . get_permalink() . '" class="button alert-modal"><div class="button_bcg"></div>Детальніше<span></span></a>';
        echo '</div>';
        echo '</div>';
        echo '<div class="wedding-item" style="background:url(' . get_the_post_thumbnail_url() . ');"></div>';
        echo '</div>'; //wedding-item-wrapper
    endwhile;
    print('</div>'); //col-md-12
    print('</div>'); //row
    print('</div>'); //container
    print('</section>'); //wedding-section
    echo '';

    wp_reset_query();
}

add_shortcode('wedding-category', 'program_func_wedding_category');
//
//
//function js_func(){
//    query_posts(array('category_name' => 'javascript', 'posts_per_page' => -6));
//    print('<section class="js-section">');
//    print('<div class="container">');
//    print('<div class="row">');
//    print('<div class="col-md-12">');
//    print('<h2 class="section-title">JavaScript</h2>');
//    while (have_posts()) : the_post();
//        echo '<a href="' . get_permalink() . '" class="js-curse-item">';
//        echo '<div class="post-image" style="background: url('.get_the_post_thumbnail_url().')"></div>';
//        echo    '<div class="js-curse-item-wrapper">';
//        echo        '<div class="js-curse-content">';
//        echo            '<div class="post-title">';
//        echo               the_title();
//        echo            '</div>';
//        echo            '<p class="post-text">';
//        echo               wp_trim_words(get_the_excerpt(), 30, '...');
//        echo            '</p>';
//        echo            '<div class="price">';
//        echo               get_post_meta(get_the_ID(), "prise", true);
//        echo            '</div>';
//        echo        '</div>';
//        echo    '</div>';
//        echo '</a>';
//    endwhile;
//    print('</div>'); //col-md-12
//    print('</div>'); //row
//    print('</div>'); //container
//    print('</section>'); //js-section
//    echo '';
//
//    wp_reset_query();
//}
//
//
//add_shortcode('js-curse', 'js_func');
//
//function js_func_category(){
//    query_posts(array('category_name' => 'javascript', 'posts_per_page' => -6));
//    print('<section class="js-section">');
//    print('<div class="container">');
//    print('<div class="row">');
//    print('<div class="col-md-12">');
//    print('<h2 class="section-title">JavaScript</h2>');
//    while (have_posts()) : the_post();
//        echo '<a href="' . get_permalink() . '" class="js-curse-item">';
//        echo '<div class="post-image" style="background: url('.get_the_post_thumbnail_url().')"></div>';
//        echo    '<div class="js-curse-item-wrapper">';
//        echo        '<div class="js-curse-content">';
//        echo            '<div class="post-title">';
//        echo               the_title();
//        echo            '</div>';
//        echo            '<p class="post-text">';
//        echo               wp_trim_words(get_the_excerpt(), 30, '...');
//        echo            '</p>';
//        echo            '<div class="price">';
//        echo               get_post_meta(get_the_ID(), "prise", true);
//        echo            '</div>';
//        echo        '</div>';
//        echo    '</div>';
//        echo '</a>';
//    endwhile;
//    print('</div>'); //col-md-12
//    print('</div>'); //row
//    print('</div>'); //container
//    print('</section>'); //htmlcss-section
//    echo '';
//
//    wp_reset_query();
//}
//
//
//add_shortcode('js-category', 'js_func_category');
//
//
//
//function blog_func(){
//    query_posts(array('category_name' => 'blog', 'posts_per_page' => -9));
//    print('<section class="blog-section">');
//        print('<div class="container">');
//            print('<div class="row">');
//                print('<div class="col-md-12">');
//                    print('<h2 class="section-title">Блог</h2>');
//                    print('<ul class="list-blog-front">');
//                        while (have_posts()) : the_post();
//                            echo '<li>';
//                            echo    '<a  href="' . get_permalink() . '" class="blog-link">';
//                            echo        '<div class="link-bg">';
//                            echo            get_the_post_thumbnail(get_the_ID(), array(400, 400));
//                            echo        '</div>';
//                            echo        '<div class="main-info">';
//                            echo            '<div class="blog-uptitle-post">';
//                            echo                get_post_meta(get_the_ID(), "uptitle", true);
//                            echo            '</div>';
//                            echo            '<div class="post-title">';
//                            echo                the_title();
//                            echo            '</div>';
//                            echo            '<p class="post-text">';
//                            echo                wp_trim_words(get_the_excerpt(), 20, '...');
//                            echo            '</p>';
//                            echo        '</div>';
//                            echo    '</a>';
//                            echo '</li>';
//                        endwhile;
//                    print('</ul>');
//                print('</div>'); //col-md-12
//            print('</div>'); //row
//        print('</div>'); //container
//    print('</section>'); //htmlcss-section
//    echo '';
//
//    wp_reset_query();
//}
//add_shortcode('blog', 'blog_func');
////
//function blog_func_category(){
//    query_posts(array('category_name' => 'blog', 'posts_per_page' => -100));
//    print('<section class="blog-section">');
//        print('<div class="container">');
//            print('<div class="row">');
//                print('<div class="col-md-12">');
//                    print('<h2 class="section-title">Блог</h2>');
//                    print('<ul class="list-blog-front">');
//                        while (have_posts()) : the_post();
//                            echo '<li>';
//                            echo    '<a  href="' . get_permalink() . '" class="blog-link">';
//                            echo        '<div class="link-bg">';
//                            echo            get_the_post_thumbnail(get_the_ID(), array(400, 400));
//                            echo        '</div>';
//                            echo        '<div class="main-info">';
//                            echo            '<div class="blog-uptitle-post">';
//                            echo                get_post_meta(get_the_ID(), "uptitle", true);
//                            echo            '</div>';
//                            echo            '<div class="post-title">';
//                            echo                the_title();
//                            echo            '</div>';
//                            echo            '<p class="post-text">';
//                            echo                wp_trim_words(get_the_excerpt(), 20, '...');
//                            echo            '</p>';
//                            echo        '</div>';
//                            echo    '</a>';
//                            echo '</li>';
//                        endwhile;
//                    print('</ul>');
//                print('</div>'); //col-md-12
//            print('</div>'); //row
//        print('</div>'); //container
//    print('</section>'); //htmlcss-section
//    echo '';
//
//    wp_reset_query();
//}
//add_shortcode('blog_category', 'blog_func_category');
////
////
//

