
$(document).ready(function () {
  "use strict";

    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });


  $('.comment-reply-title').text('Залишити коментар');
  $('.comment-reply-title small a').text('Сказувати коментар');

  $('#sidebar-right .recent-posts-widget-with-thumbnails ul li').click(function () {
     $(this).find('a').trigger('click')
  });

  var menu = $('.menu-mobile');
  var menuActive = false;
  var header = $('.header-fixed');

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
  objectFitImage();
  setHeader();
  initMenu();
  initHomeSlider();


  $(window).on('resize', function () {
    setHeader();
  });

  $(document).on('scroll', function () {
    setHeader();
  });

  function objectFitImage() {
    $('.header-page .post-thumbnail').each(function () {
      var linkImage = $(this).find('img').prop('src');
      if (linkImage) {
        $(this).css('background', 'url(' + linkImage + ') no-repeat');
      }
    })
  }

  function setHeader() {
    if (window.innerWidth < 992) {
      if ($(window).scrollTop() > 100) {
        header.addClass('scrolled');
      }
      else {
        header.removeClass('scrolled');
      }
    }
    else {
      if ($(window).scrollTop() > 100) {
        header.addClass('scrolled');
      }
      else {
        header.removeClass('scrolled');
      }
    }
    if (window.innerWidth > 991 && menuActive) {
      closeMenu();
    }
  }

  function initMenu() {
    if ($('.hamburger').length && $('.menu-mobile').length) {
      var hamb = $('.hamburger');
      var close = $('.menu-mobile');

      hamb.on('click', function () {
        if (!menuActive) {
          openMenu();
        }
        else {
          closeMenu();
        }
      });

      close.on('click', function () {
        if (!menuActive) {
          openMenu();
        }
        else {
          closeMenu();
        }
      });
    }
  }

  function openMenu() {
    menu.addClass('active');
    menuActive = true;
  }

  function closeMenu() {
    menu.removeClass('active');
    menuActive = false;
  }

  function initHomeSlider() {
    if ($('.home-slider').length) {
      var homeSlider = $('.home-slider');

      homeSlider.owlCarousel(
        {
          items: 1,
          loop: true,
          autoplay: true,
          smartSpeed: 1200
        });

      if ($('.slide-prev').length) {
        var prev = $('.slide-prev');
        prev.on('click', function () {
          homeSlider.trigger('prev.owl.carousel');
        });
      }
      if ($('.slide-next').length) {
        var next = $('.slide-next');

        next.on('click', function () {
          homeSlider.trigger('next.owl.carousel');
        });
      }
    }
  }

    $(".form-group .wpcf7-form-control").focus(function () {
        $(this).parents(".form-group").addClass("has-label")
    }).blur(function () {
      console.log('red')
      if($(this).val() == ""){
          $(this).parents(".form-group").removeClass("has-label")
      }
    });

});